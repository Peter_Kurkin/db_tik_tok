BEGIN TRANSACTION;
CREATE TABLE users
(
    id       SERIAL PRIMARY KEY,
    email    VARCHAR(100),
    password VARCHAR(100),
    username VARCHAR(100),
    UNIQUE (email)
);
CREATE TABLE video
(
    id             SERIAL PRIMARY KEY,
    count_likes    INTEGER,
    count_comments INTEGER,
    user_id        INTEGER,
    FOREIGN KEY (user_id) REFERENCES users (id)
);
CREATE TABLE chat
(
    id        SERIAL PRIMARY KEY,
    user_id_1 INTEGER,
    user_id_2 INTEGER,
    message   VARCHAR(1000),
    date      DATE,
    FOREIGN KEY (user_id_1) references users (id),
    FOREIGN KEY (user_id_2) REFERENCES users (id)
);
CREATE TABLE comments
(
    id         SERIAL PRIMARY KEY,
    user_id    INTEGER,
    video_id   INTEGER,
    text       varchar(256),
    count_like INTEGER,
    date       DATE,
    FOREIGN KEY (user_id) REFERENCES users (id),
    FOREIGN KEY (video_id) REFERENCES video (id)
);

COMMIT;
