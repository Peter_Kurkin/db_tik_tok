import sqlite3
import random
import string
from datetime import date

COUNT = 10001


def generate_random_string(length):
    letters = string.ascii_lowercase
    rand_string = ''.join(random.choice(letters) for i in range(length))
    return rand_string


def random_date():
    start_date = date.today().replace(day=1, month=1).toordinal()
    end_date = date.today().toordinal()
    random_day = date.fromordinal(random.randint(start_date, end_date))
    return random_day


def main():
    with open("init_data.sql", 'w') as file:
        for i in range(1, COUNT):
            file.write(
                f'INSERT INTO users (id, email, username, password) VALUES ({i}, '
                f'"{generate_random_string(6) + str(i) + "@mail.ru"}", "{generate_random_string(8)}", '
                f'"{generate_random_string(8)}");\n')
        for i in range(1, COUNT):
            file.write(
                f'INSERT INTO video (id, count_likes, count_comments, user_id) '
                f'VALUES ({i}, {random.randrange(1, 10000)}, {random.randrange(1, 1000)}, '
                f'{random.randrange(1, 10000)});\n')
        for i in range(1, COUNT):
            file.write(f'INSERT INTO comments (id, user_id, video_id, text, count_like, date) '
                       f'VALUES ({i}, {random.randrange(1, 10000)}, {random.randrange(1, 10000)}, '
                       f'"{generate_random_string(100)}", {random.randrange(1, 10000)}, {random_date()});\n')
        for i in range(1, COUNT):
            file.write(
                f'INSERT INTO chat (id, user_id_1, user_id_2, message, date) '
                f'VALUES ({i}, {random.randrange(1, 10000)}, {random.randrange(1, 10000)},'
                f'"{generate_random_string(200)}", {random_date()};\n')


if __name__ == '__main__':
    main()
